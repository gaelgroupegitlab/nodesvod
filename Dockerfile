# Utilisez l'image de base appropriée pour votre application
FROM node:latest

# Répertoire de travail
WORKDIR /app

# Copie des fichiers de l'application dans le conteneur
COPY . .

# Installation des dépendances et compilation
RUN npm ci
RUN ./node_modules/.bin/eslint app.js

CMD node server.js
